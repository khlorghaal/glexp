package fluid;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL32;
import org.lwjgl.util.vector.Matrix4f;

import src.Environment;
import src.IProgram;
import src.Target;

public class Fluid implements IProgram{
	public static void main(String[] args){
		new Environment( new Fluid() ).run();
	}
	
	int prog_r, prog_advect, prog_divergence, prog_laplace, prog_grad, prog_force;
	int uloc_a_step, uloc_d_step;
	
	
	static Target v, p, div, dye;
	
	public final static int w= 512;
	
	int vbo, vao;
	
	
	@Override
	public void init(){		
		final String pass= ""
				+ "#version 440\n"
				+ "layout(location=0) in vec2 pi;"
				+ "layout(location=1) in vec2 vui;"
				+ ""
				+ "smooth out vec2 uv;"
				+ "void main(){"
				+ "    gl_Position= vec4(pi,0,1);"
				+ "    uv= vui;"
				+ "}";
		
		
		prog_r= Environment.program(pass, ""
				+ "#version 440\n"
				+ "layout(binding=0) uniform sampler2D dyes;"
				+ "layout(binding=1) uniform sampler2D vs;"
				+ ""
				+ "smooth in vec2 uv;"
				+ "out vec3 color;"
				+ "void main(){"
				+ "    float dye= texelFetch(dyes, ivec2(uv*textureSize(vs,0)), 0).x;"
				+ "    vec2 v= texelFetch(vs, ivec2(uv*textureSize(vs,0)), 0).xy;"
//				+ "    color= vec3(.5, v.yx/2+.5);"
				+ "    color= vec3(0, abs(v.yx)*.0) +.3;"
				+ "    color+= max(-.4, dye);"
//				+ "    color+= abs(dye)*mix(vec3(1,0,0),vec3(0,0,1),dye);"
//				+ "    color= vec3(0, normalize(v.yx+.01)/2+.5);"
//				+ "    color*= max(0, abs(dye));"
				+ "}");
		
		
		prog_advect= Environment.program(pass, ""
				+ "#version 440\n"
				+ "layout(binding=0) uniform sampler2D vi;"
				+ "layout(binding=1) uniform sampler2D dyei;"
				+ "uniform float step;"
				+ ""
				+ "smooth in vec2 uv;"
				+ "layout(location=0) out vec2 v;"
				+ "layout(location=1) out float dye;"
				+ "void main(){"
				+ "    ivec2 c= ivec2(gl_FragCoord.xy);"
				+ "    vec2 adv= -texelFetch(vi, c, 0).xy;"
				+ "    adv= (vec2(c)+.5-adv) / textureSize(vi,0);"
				+ "    vec2 d= .5 / textureSize(vi,0);"
				+ "    vec2 u= vec2(d.x,d.y);"
				+ "    vec2 dn= vec2(d.x,-d.y);"
				+ "    vec2 l= vec2(-d.x,d.y);"
				+ "    vec2 r= vec2(d.x,d.y);"
				+ "    v= texture(vi, adv, 0).xy;"
				+ "    v+=texture(vi, adv+u, 0).xy;"
				+ "    v+=texture(vi, adv+dn, 0).xy;"
				+ "    v+=texture(vi, adv+l, 0).xy;"
				+ "    v+=texture(vi, adv+r, 0).xy;"
				+ "    v/=5;"
				+ "    dye= texture(dyei, v/textureSize(vi,0), 0).x;"
				+ "}");
		
		
		prog_divergence= Environment.program(pass, ""
				+ "#version 440\n"
				+ "uniform sampler2D vi;"
				+ ""
				+ "smooth in vec2 uv;"
				+ "out float d;"
				+ "void main(){"
				+ "    ivec2 c= ivec2(gl_FragCoord.xy);"
				+ "    ivec2 up=   c+ivec2( 0, 1);"
				+ "    ivec2 down= c+ivec2( 0,-1);"
				+ "    ivec2 left= c+ivec2(-1, 0);"
				+ "    ivec2 right=c+ivec2( 1, 0);"
				+ ""
				+ "    vec2 vup= texelFetch(vi, up, 0).xy;"
				+ "    vec2 vdown= texelFetch(vi, down, 0).xy;"
				+ "    vec2 vleft= texelFetch(vi, left, 0).xy;"
				+ "    vec2 vright= texelFetch(vi, right, 0).xy;"
				+ "    d= (vright.x-vleft.x + vup.y-vdown.y)/2;"
				+ "}");
		
		prog_laplace= Environment.program(pass, ""
				+ "#version 440\n"
				+ "layout(binding=0) uniform sampler2D pi;"
				+ "layout(binding=1) uniform sampler2D di;"
				+ ""
				+ "smooth in vec2 uv;"
				+ "out float po;"
				+ "void main(){"
				+ "    ivec2 c= ivec2(gl_FragCoord.xy);"
				+ "    ivec2 up=   c+ivec2( 0, 1);"
				+ "    ivec2 down= c+ivec2( 0,-1);"
				+ "    ivec2 left= c+ivec2(-1, 0);"
				+ "    ivec2 right=c+ivec2( 1, 0);"
				+ ""
				+ "    float dc= texelFetch(di, up, 0).x;"
				+ "    float pup= texelFetch(pi, up, 0).x;"
				+ "    float pdown= texelFetch(pi, down, 0).x;"
				+ "    float pleft= texelFetch(pi, left, 0).x;"
				+ "    float pright= texelFetch(pi, right, 0).x;"
				+ "    po= (pup+pdown+pleft+pright - dc)/4;"
				+ "}");
		
		prog_grad= Environment.program(pass, ""
				+ "#version 440\n"
				+ "layout(binding=0) uniform sampler2D pi;"
				+ "layout(binding=1) uniform sampler2D vi;"
				+ "uniform float step;"
				+ ""
				+ "smooth in vec2 uv;"
				+ "layout(location=0) out vec2 vo;"
				+ "void main(){"
				+ "    ivec2 c= ivec2(gl_FragCoord.xy);"
				+ "    ivec2 up=   c+ivec2( 0, 1);"
				+ "    ivec2 down= c+ivec2( 0,-1);"
				+ "    ivec2 left= c+ivec2(-1, 0);"
				+ "    ivec2 right=c+ivec2( 1, 0);"
				+ ""
				+ "    float pup= texelFetch(pi, up, 0).x;"
				+ "    float pdown= texelFetch(pi, down, 0).x;"
				+ "    float pleft= texelFetch(pi, left, 0).x;"
				+ "    float pright= texelFetch(pi, right, 0).x;"
				+ "    vo= texelFetch(vi, c, 0).xy;"
				+ "    vo-= vec2( pright-pleft , pup-pdown )/2;"
				+ "}");
		
		prog_force= Environment.program(""
				+ "#version 440\n"
				+ ""
				+ "layout(location=0) in vec2 pos;"
				+ "smooth out vec2 uv;"
				+ "void main(){"
				+ "    gl_Position= vec4(pos, 0,1);"
				+ "    uv= vec2(1);"
				+ "}",""
						+"#version 440\n"
						+ "uniform vec2 f;"
						+ ""
						+ "smooth in vec2 uv;"
						+ "out vec2 force;"
						+ "void main(){"
						+ "    force= f;"
						+ "}");
		
		//		"#version 440\n"
		//		+ ""
		//		+ "void main(){"
		//		+ ""
		//		+ "}"
		
		
		uloc_a_step= GL20.glGetUniformLocation(prog_advect, "step");
		uloc_d_step= GL20.glGetUniformLocation(prog_divergence, "step");
		
		
		
		
		float[] pressure= new float[w*w];
		float[] velocity= new float[w*w*2];		
		float[] dy= new float[w*w];
		for(int x=w*7/16; x<w*9/16; x++){
			for(int y=w*7/16; y<w*9/16; y++){
//				dy[x+y*w]= 1;
				for(int c=0; c!=2; c++){
//					if(c==0)
//						velocity[c+2*(x+y*w)]= 2f;
				}
			}
		}
		//		for(int x=w*7/16; x<w*9/16; x++){
		//			for(int y=w*7/16; y<w*9/16; y++){
		//				for(int c=0; c!=2; c++){
		//					if(c==1)
		//						velocity[c+2*(x+y*w)]= 2f;
		//				}
		//			}
		//		}
		
		v= new Target(velocity, 2);
		p= new Target(pressure, 1);
		div= new Target(null, 1);
		dye= new Target(dy, 1);

		for(int i=0; i!=2; i++){
			v.fboBind();
			GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT1, dye.tex(), 0);
			IntBuffer ib= BufferUtils.createIntBuffer(2);
			ib.put(GL30.GL_COLOR_ATTACHMENT0); ib.put(GL30.GL_COLOR_ATTACHMENT1); ib.clear();
			GL20.glDrawBuffers(ib);
			v.pingpong();
			dye.pingpong();
		}
		dye.pingpong();
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
		
		vbo= GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		FloatBuffer fbuf= BufferUtils.createFloatBuffer(4*2*2);
		fbuf.put(new float[]{
				-1,-1, 0,0,
				1,-1, 1,0,
				-1, 1, 0,1,
				1, 1, 1,1,
		});
		fbuf.clear();
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fbuf, GL15.GL_STATIC_DRAW);
		
		vao= GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vao);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 16, 0);
		GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 16, 8);
		
		
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(false);
		GL11.glDisable(GL11.GL_BLEND);
		
		Environment.checkerr();
	}
	
	@Override
	public void render(Matrix4f cam, Object[] unis){
		GL20.glUseProgram(prog_r);
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, v.tex());
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, dye.tex());
		
		GL30.glBindVertexArray(vao);
		GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		
	}
	
	@Override
	public void transform(Object[] unis){
		int iter= 64;
		float step= 1f/iter;
		//for(int i=0; i!=iter; i++)
		transform_(unis, step);
	}
	void transform_(Object[] unis, float step){
		GL30.glBindVertexArray(vao);
		GL11.glViewport(0, 0, w, w);
		
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		
		GL20.glUseProgram(prog_advect);
		GL20.glUniform1f(uloc_a_step, step);
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		dye.texBind();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		v.texBind();
		v.fboBind();
		GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		v.pingpong();
		
		if(Mouse.isButtonDown(0) || Mouse.isButtonDown(1)){
			GL20.glUseProgram(prog_force);
			GL20.glUniform2f(GL20.glGetUniformLocation(prog_force, "f"), Mouse.isButtonDown(0)?.5f:-.5f, 0);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
			GL11.glPointSize(5);
			GL11.glBegin(GL11.GL_POINTS);
			GL11.glVertex2f((float)Mouse.getX()/Display.getWidth()*2-1, 
					(float)Mouse.getY()/Display.getHeight()*2-1);
			GL11.glEnd();
			GL11.glDisable(GL11.GL_BLEND);
		}
		
		GL20.glUseProgram(prog_divergence);
		GL20.glUniform1f(uloc_d_step, step);
		v.texBind();
		div.fboBind();
		GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		div.pingpong();
		
		
		GL20.glUseProgram(prog_laplace);
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		div.texBind();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		p.fboBind();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		p.pingpong();
		p.fboBind();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		for(int i=0; i!=32; i++){
			p.texBind();
			p.fboBind();
			GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
			p.pingpong();
		}
		
		GL20.glUseProgram(prog_grad);
		GL20.glUniform1f(uloc_d_step, step);
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		v.texBind();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		p.texBind();
		v.fboBind();		
		GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		v.pingpong();
		
		
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
		GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
	}
}
