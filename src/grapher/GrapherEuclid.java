package grapher;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL41.glVertexAttribLPointer;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Matrix4f;

import src.Environment;
import src.IProgram;
import src.VertexFactory;
import src.VertexFactory.SimplexSheet;

public class GrapherEuclid implements IProgram{
	public static void main(String[] args){
		new Environment( new GrapherEuclid() ).run();
	}
	
	int prog, vao, vbo, ibo;
	@Override
	public void init(){
		prog= Environment.program(
				"#version 430\n"
						+ "layout(location=0) uniform mat4x4 mvp;"
						+ "layout(location=1) uniform vec2 gsiz;"
						+ "layout(location=2) uniform float t;"
						+ ""
						+ "layout(location=0) in vec2 xy;"
						+ "float f(vec2 xy){"
						//				+ " if(length(xy)>1)"
						//				+ "  return sqrt(-1);"
						//						+ " return cos((length(xy)+t/4)*3.14*8 )/3.14/16 + sqrt(1-length(xy)*length(xy));"
						+ " return 0;"
						+ "}"
						+ ""
						+ "flat out vec3 normv;"
						+ "void main(){"
						+ " vec3 pos= vec3(xy, f(xy));"
						+ " gl_Position= vec4(mvp*vec4(pos.xzy,1));"
						+ " "
						+ " vec2 d= .1/gsiz;"
						+ " float dx= d.x;"
						+ " float dy= d.y;"
						+ " vec2 fdxdy= vec2( f(xy+vec2(dx,0)), f(xy+vec2(0,dy)) );"
						+ " vec2 df= (fdxdy-pos.z)/vec2(dx,dy);"
						+ " normv= normalize(vec3( -df, 1));"
						+ "}"
						,
						"#version 430\n"
								+ "layout(triangles) in;"
								+ "layout(line_strip, max_vertices=4) out;"
								+ ""
								+ "flat in vec3 normv[];"
								+ "smooth out vec3 norm;"
								+ "void main(){"
								+ " const int inds[4]= {0, 1, 2, 0};"
								+ " for(int i=0; i!=4; i++){"
								+ "  int j= inds[i];"
								+ "  gl_Position= gl_in[j].gl_Position;"
								+ "  norm= normv[j];"
								+ "  EmitVertex();"
								+ " }"
								+ " EndPrimitive();"
								+ "}"
								,
								"#version 430\n"
										+ "in vec3 norm;"
										+ "out vec3 color;"
										+ "void main(){"
										+ " float d= gl_FragCoord.z/gl_FragCoord.w;"
										+ " d= sqrt(d);"
										+ " vec3 a= vec3(0, .3, 1);"
										+ " vec3 b= vec3(.5, 0, 1);"
										+ " color= mix(a,b, d/10);"
										+ ""
										+ " color= abs(norm);"
										//				+ " color= vec3(.9);"
										+ "}");
		
		vao= glGenVertexArrays();
		vbo= glGenBuffers();
		ibo= glGenBuffers();
		
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, false, 2*4, 0);
		
		sheet= new SimplexSheet(16);
		
		FloatBuffer fbuf= BufferUtils.createFloatBuffer(sheet.verts.length);
		IntBuffer ibuf= BufferUtils.createIntBuffer(sheet.inds.length);
		fbuf.put(sheet.verts);
		fbuf.flip();
		glBufferData(GL_ARRAY_BUFFER, fbuf, GL_STATIC_DRAW);
		ibuf.put(sheet.inds);
		ibuf.flip();
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ibuf, GL_STATIC_DRAW);
		
	}
	
	public SimplexSheet sheet;
	
	@Override
	public void render(Matrix4f cam, Object[] unis){
		glUseProgram(prog);
		
		FloatBuffer fbuf= BufferUtils.createFloatBuffer(4*4);
		cam.store(fbuf);
		fbuf.flip();
		glUniformMatrix4(0, false, fbuf);
		glUniform2f(1, sheet.gw, sheet.gh);
		float t= (System.currentTimeMillis()%10000)/1000f;
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE))
			t=0;
		//		t= 2*(.5f-Math.abs(t-.5f));
		glUniform1f(2, t);
		
		glBindVertexArray(vao);
		glPointSize(4);
		glDrawElements(GL_TRIANGLES, sheet.tris*3, GL_UNSIGNED_INT, 0);
		//		glDrawArrays(GL_POINTS, 0, sheet.count);
	}
	
	@Override
	public void transform(Object[] unis){
		// TODO Auto-generated method stub
		
	}
	
}
