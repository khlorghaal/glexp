package src;

import org.lwjgl.util.vector.Matrix4f;

public interface IProgram{
	public void init();
	
	public void render(Matrix4f cam, Object[] unis);
	
	public void transform(Object[] unis);
}
