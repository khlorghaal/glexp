package src;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL32;

import fluid.Fluid;

public class Target{
		private int ping=0, pong=1;
		private final int[] fbo= new int[2], tex= new int[2];
		public Target(float[] data, int components){			
			FloatBuffer fbuf= null;
			if(data!=null){
				fbuf= BufferUtils.createFloatBuffer(data.length);
				fbuf.clear();
				fbuf.put(data);
				fbuf.clear();
			}
			int internal, external;
			switch(components){
				case 1:
					internal= GL30.GL_R32F;
					external= GL11.GL_RED;
					break;
				case 2:
					internal= GL30.GL_RG32F;
					external= GL30.GL_RG;
					break;
				case 3:
					internal= GL30.GL_RGBA32F;//RGB render not supported
					external= GL11.GL_RGBA;
					break;
				default:
					internal= external= -1;
			}
			for(int i=0; i!=2; i++){
				tex[i]= GL11.glGenTextures();
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex[i]);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
				GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, internal, Fluid.w, Fluid.w, 0, external, GL11.GL_FLOAT, fbuf);
				
				fbo[i]= GL30.glGenFramebuffers();
				GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fbo[i]);
				GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, tex[i], 0);
//				fbuf= null;
			}
		}
		public int tex(){
			return tex[ping];
		}
		public void texBind(){
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex());
		}
		public int fbo(){
			return fbo[pong];
		}
		public void fboBind(){
			GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fbo() );
		}
		public void pingpong(){
			ping= ping==1? 0:1;
			pong= pong==1? 0:1;			
		}
	}