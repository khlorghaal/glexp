package src;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glFinish;
import static org.lwjgl.opengl.GL11.glGetError;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_POINT_SPRITE;
import static org.lwjgl.opengl.GL20.GL_VERTEX_PROGRAM_POINT_SIZE;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;
import static org.lwjgl.opengl.GL40.GL_TESS_CONTROL_SHADER;
import static org.lwjgl.opengl.GL40.GL_TESS_EVALUATION_SHADER;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class Environment implements Runnable{
	
	final Profiler
	pr= new Profiler("render"),
	pt= new Profiler("transform");

	final IProgram p;
	public Environment(IProgram p){
		this.p= p;
	}	
	
	@Override
	public void run(){
		try {
			Display.create();
			Display.setResizable(true);
		} catch (LWJGLException e) {e.printStackTrace();}
		
		checkerr();
		Profiler.setPrinting(false);
		
		initGL();
		boolean fullscreen= false;
		
		/////////////////////////////////////
		while(true){
			
			if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) || Display.isCloseRequested())
				break;
			
			//screen adjustment
			if(Display.wasResized())
				glViewport(0, 0, Display.getWidth(), Display.getHeight());
			if(Keyboard.isKeyDown(Keyboard.KEY_F11))
				try {
					fullscreen=!fullscreen;
					Display.setFullscreen(fullscreen);
					Display.update();
					glViewport(0, 0, Display.getWidth(), Display.getHeight());
				} catch (LWJGLException e) {e.printStackTrace();}
			
			update();
			
			glFinish();
			Display.update();
			Display.sync(60);
		}
		//////////////////////////////////////
		
		Display.destroy();
	}
	void initGL(){
		glViewport(0, 0, Display.getWidth(), Display.getHeight());
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glEnable(GL_POINT_SPRITE);
		glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
		
		p.init();
	}
	
	public static void checkerr(){
		final int s= glGetError();
		if(s!=0)
			System.err.printf("0x%x "+GLU.gluErrorString(s)+'\n', s);
	}
	
	private static final Vector3f X= new Vector3f(1, 0, 0);
	private static final Vector3f Y= new Vector3f(0, 1, 0);
	private final Vector3f pos= new Vector3f(0,0,0);
	private float zoom= 1.1f, rx=0, ry=0;
	
	private float mdx, mdy, mx, my;
	
	void update(){
		final float idw= 1f/Display.getWidth();
		mdx= Mouse.getDX()*idw;
		mdy= Mouse.getDY()*idw;
		mx= (Mouse.getX()-Display.getWidth()/2)*idw;
		my= (Mouse.getY()-Display.getHeight()/2)*idw;
		
		
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		pr.start();
		render();
		glFinish();
		pr.end().printTime();
		
		if(!Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
			pt.start();
			float a= .00015f*zoom;
			float agx=0, agy=0, agz=0;
			if(Keyboard.isKeyDown(Keyboard.KEY_W))
				agy+= a;
			if(Keyboard.isKeyDown(Keyboard.KEY_A))
				agx-= a;
			if(Keyboard.isKeyDown(Keyboard.KEY_S))
				agy-= a;
			if(Keyboard.isKeyDown(Keyboard.KEY_D))
				agx+= a;
			if(Keyboard.isKeyDown(Keyboard.KEY_Q))
				agz+= a;
			if(Keyboard.isKeyDown(Keyboard.KEY_E))
				agz-= a;
			
			Object[] unis= new Object[]{1f, .1f, agx,agy,agz, mx,my, mdx,mdy};

			p.transform(unis);
			
			glFinish();
			pt.end().printTime();
		}
	}
	
	void render() {
		Matrix4f cam= new Matrix4f();
		
		final float aspectinverse= (float)Display.getHeight() / (float)Display.getWidth();
		cam.m00= aspectinverse;
		//		cam.scale(new Vector3f(.1f, .1f, .1f));
		//		cam.m33= 4f;
		
		//////
		//zoom
		final float zoomyness= 1.001f;
		int exp= Mouse.getDWheel();
		boolean neg= exp<0;
		for(int i=0; i!=exp; i+= neg? -1:1)
			zoom*= neg? zoomyness : 1/zoomyness;
		cam.translate(new Vector3f(0, 0, zoom));
		
		//////////
		//rotation
		if(Mouse.isButtonDown(0)){
			rx+= mdx;
			ry+= mdy;
		}
		if(Mouse.isButtonDown(1)){
			rx+= mdx*4;
			ry+= mdy*4;
		}
		cam.rotate(ry, X);
		cam.rotate(-rx, Y);
		//		glDepthRange( (float)Mouse.getX()/Display.getWidth() -.5f , (float)Mouse.getY()/Display.getHeight() );
		//				cam.rotate((float)(System.currentTimeMillis()%100000l)/5000f, X);
		//				cam.rotate((float)(System.currentTimeMillis()%100000l)/5000f, Y);
		
		//////////
		//position
		final float mod= Keyboard.isKeyDown(Keyboard.KEY_RSHIFT)? 3:1;
		if(Keyboard.isKeyDown(Keyboard.KEY_LEFT))
			pos.x-= .015*mod*zoom;
		if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT))
			pos.x+= .015*mod*zoom;
		if(Keyboard.isKeyDown(Keyboard.KEY_UP))
			pos.y-= .015*mod*zoom;
		if(Keyboard.isKeyDown(Keyboard.KEY_DOWN))
			pos.y+= .015*mod*zoom;
		
		//		System.out.println(pos.z);
		cam.translate(pos);
		
		///////////////////////
		//perspective and depth		
		cam.m03= cam.m02;
		cam.m13= cam.m12;
		cam.m23= cam.m22;
		cam.m33= cam.m32;

		final float FAR= 100f;
		cam.m02/= FAR;
		cam.m12/= FAR;
		cam.m22/= FAR;
		cam.m32/= FAR;

		p.render(cam, null);
		
		//		final Vector4f v= new Vector4f(0,0,1,1);
		//		Matrix4f.transform(cam, v, v);
		//		System.out.println(v);
		
		checkerr();
	}
	
	public static int program(String vshsrc){
		int ret= glCreateProgram();
		
		int vsh= glCreateShader(GL_VERTEX_SHADER);
		
		glShaderSource(vsh, vshsrc);
		
		glCompileShader(vsh);
		System.out.println( glGetShaderInfoLog(vsh, 300) );
		
		glAttachShader(ret, vsh);
						
		return ret;
	}
	
	public static int program(String vshsrc, String fshsrc){
		int ret= glCreateProgram();
		shader(ret, GL_VERTEX_SHADER, vshsrc);
		shader(ret, GL_FRAGMENT_SHADER, fshsrc);
		glLinkProgram(ret);
		return ret;
	}
	public static int program(String vshsrc, String gshsrc, String fshsrc){
		int ret= glCreateProgram();
		shader(ret, GL_VERTEX_SHADER, vshsrc);
		shader(ret, GL_GEOMETRY_SHADER, gshsrc);
		shader(ret, GL_FRAGMENT_SHADER, fshsrc);
		glLinkProgram(ret);
		return ret;
	}
	public static int program(String vshsrc, String tcshsrc, String teshsrc, String fshsrc){
		int ret= glCreateProgram();
		shader(ret, GL_VERTEX_SHADER, vshsrc);
		shader(ret, GL_TESS_CONTROL_SHADER, tcshsrc);
		shader(ret, GL_TESS_EVALUATION_SHADER, teshsrc);
		shader(ret, GL_FRAGMENT_SHADER, fshsrc);
		glLinkProgram(ret);
		return ret;
	}
	public static int program(String vshsrc, String tcshsrc, String teshsrc, 
			String gshsrc, String fshsrc){
		int ret= glCreateProgram();
		shader(ret, GL_VERTEX_SHADER, vshsrc);
		shader(ret, GL_TESS_CONTROL_SHADER, tcshsrc);
		shader(ret, GL_TESS_EVALUATION_SHADER, teshsrc);
		shader(ret, GL_GEOMETRY_SHADER, gshsrc);
		shader(ret, GL_FRAGMENT_SHADER, fshsrc);
		glLinkProgram(ret);
		return ret;
	}
	
	public static void shader(int prog, int stage, String src){
		glAttachShader(prog, shader(stage, src));
	}
	public static int shader(int stage, String src){
		int ret= glCreateShader(stage);
		glShaderSource(ret, src);
		glCompileShader(ret);
		System.out.println( glGetShaderInfoLog(ret, 300) );
		return ret;
	}
}
