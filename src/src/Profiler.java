package src;

public class Profiler {
	private String tag;
	
	private long starttime;
	private long endtime;
	private long dtime;
	
	public Profiler(String tag) {
		this.tag= tag;
	}

	public Profiler start(){
		starttime= System.nanoTime();
		return this;
	}
	
	public Profiler end(){
		endtime= System.nanoTime();
		dtime= endtime-starttime;
		return this;
	}
	
	private static boolean print= true;
	public static void setPrinting(boolean state){print= state;}
	public Profiler printTime(){
		if(print)
			System.out.println(tag+" "+dtime/1000+"us");
		return this;
	}
	 
}
