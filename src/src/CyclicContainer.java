package src;

public class CyclicContainer {
	public interface MemoryOperator{
		public void use(int start, int count);
	}
	MemoryOperator op;
	
	public CyclicContainer(MemoryOperator op){
		this.op= op;
	}
	
	public int newest, memsiz, size;

	public void use(){		
		if(newest>size){
			//coherent
			//_____________________
	 		//          ^    ^
			//        n-s    n   
			op.use(newest-size, size);
		}
		else{
			//fragmented
			// ____________________
			//^   ^            ^   ^
			//0   n       ms-rem   ms
			op.use(0, newest);
			int rem= size-newest;
			op.use(memsiz-rem, rem);
		}
	}
	public void add(){
		newest++;
		if(newest==memsiz)
			newest=0;

		if(size!=memsiz)
			size++;
	}
	public void decay(){
		size--;
		if(size<0)
			size=0;
	}
}
