package src;

public class VertexFactory{
	
	public static class SimplexSheet{
		public final int gw, gh, count, tris;
		public final float[] verts;
		public final int[] inds;
		public SimplexSheet(int w){
			gw= w;
			gh= (int)(Math.ceil( gw/(Math.sqrt(3)/2) )); 
			count= gw*gh;
			tris= (gw-1)*(gh-1)*2;
			
			verts= new float[count*2];
			inds= new int[tris*3];
			
			verts(verts, gw, gh);
			inds(inds, gw, gh);
		}
		
		
		public static void verts(float[] verts, int w, int h){
			final float SIN60= (float)(Math.sqrt(3))/2;
			final float COS60= .5f;
			for(int i=0; i!=w*h; i++){
				//make simplex verts
				int x= i%w;
				int y= i/w; 
				verts[i*2]= ( x + (y&1)*COS60 )/w*2-1;//offset odd rows
				verts[i*2+1]= ( y*SIN60 )/w*2-1;
			}
		}
		public static void inds(int[] inds, int w, int h){
			int v= 0;
			for(int i=0; i<inds.length-5; i+=6){
				if((v/w&1)==0){
					//v+w_____v+w+1
					//   |\   |
					//   | \  |
					//   |  \ |
					//  v|___\|v+1
					inds[i]= v;
					inds[i+1]= v+1;
					inds[i+2]= v+w;
					
					inds[i+3]= v+1;
					inds[i+4]= v+w;
					inds[i+5]= v+w+1;
				}
				else{//odd y rows would otherwise form isoceles
					//v+w_____v+w+1
					//   |   /|
					//   |  / |
					//   | /  |
					//  v|/___|v+1
					inds[i]= v;
					inds[i+1]= v+w+1;
					inds[i+2]= v+w;
					
					inds[i+3]= v;
					inds[i+4]= v+1;
					inds[i+5]= v+w+1;
				}
				
				v++;
				if((v+1)%w==0)
					v++;
			}
			
		}
	}
	
	
}
