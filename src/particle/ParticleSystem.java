package particle;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import src.Environment;
import src.IProgram;

/**Designed for use with minecraft*/
public class ParticleSystem implements IProgram{
	public static void main(String[] args){
		new Environment(
				new ParticleSystem(Renderer.distal, new TransformOrbit(), 1000)
				).run();
	}
	
	final Renderer r;
	final TransformFeedback t;
	final int capacity;
	/**Must be constructed from a thread with render context*/
	public ParticleSystem(Renderer r, TransformFeedback t, int capacity){
		this.r= r;
		this.t= t;
		r.psys= t.psys= this;
		
		this.capacity= capacity;
	}
	@Override
	public void init(){
		buf= BufferUtils.createByteBuffer(capacity*STRIDE);
		initBufferOBjects();
		r.init();
		t.init();		
	}

	//all writes should be directed at vbo, tvbo is only for the xform prog
	int 
	vbo, tvbo, vbolen, vbocount, 
	vao, tvao;
	ByteBuffer buf;

	public static final int
	POSCOUNT= 3, POSSIZE= POSCOUNT*Float.SIZE,
	VCOUNT= 3, VSIZE= VCOUNT*Float.SIZE;
	public final static int STRIDE= POSSIZE + VSIZE;
	private void initBufferOBjects(){
		vao= GL30.glGenVertexArrays();
		vbo= GL15.glGenBuffers();
		tvao= GL30.glGenVertexArrays();
		tvbo= GL15.glGenBuffers();

		GL30.glBindVertexArray(vao);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buf, GL15.GL_DYNAMIC_DRAW);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, STRIDE, 0);
		GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, STRIDE, POSSIZE);

		GL30.glBindVertexArray(tvao);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, tvbo);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buf, GL15.GL_DYNAMIC_DRAW);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, STRIDE, 0);
		GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, STRIDE, POSSIZE);
	}
	
	
	
	@Override
	public void render(Matrix4f modelviewprojection, Object[] uniforms){
		GL20.glUseProgram(r.prog);

		final FloatBuffer cambuf= BufferUtils.createFloatBuffer(4*4);		
		cambuf.clear();
		modelviewprojection.store(cambuf);
		cambuf.clear();
		
		r.setUniforms(cambuf, uniforms);
		
		final ByteBuffer b= BufferUtils.createByteBuffer(STRIDE*4);

		b.putFloat(1); b.putFloat(1); b.putFloat(1);
		b.putFloat(0); b.putFloat(0); b.putFloat(0);
		
		b.putFloat(10); b.putFloat(10); b.putFloat(10);
		b.putFloat(0); b.putFloat(0); b.putFloat(0);
		
		b.putFloat(100); b.putFloat(100); b.putFloat(100);
		b.putFloat(0); b.putFloat(0); b.putFloat(0);
		
		b.putFloat(1); b.putFloat(1); b.putFloat(0);
		b.putFloat(0); b.putFloat(0); b.putFloat(0);
		b.clear();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, b);
		
		GL30.glBindVertexArray(vao);
		GL11.glDrawArrays(GL11.GL_POINTS, 0, 50);
	}
	@Override
	public void transform(Object[] uniforms){
		t.invoke(uniforms);
		
	}


	public void addParticle(float[] pos, float[] v){addParticle(pos[0], pos[1], pos[2], v[0], v[1], v[2]);}
	public void addParticle(Vector3f pos, Vector3f v){addParticle(pos.x, pos.y, pos.z, v.x, v.y, v.z);}
	public void addParticle(float x, float y, float z, float vx, float vy, float vz){
		final FloatBuffer buf= BufferUtils.createFloatBuffer(3+3);
		buf.put(x);//TODO
		buf.put(y);
		buf.put(z);
		buf.put(vx);
		buf.put(vy);
		buf.put(vz);
		buf.clear();
		addParticle(buf);
	}
	public void addParticle(FloatBuffer buf){
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, STRIDE, buf);		
		//TODO
	}
		
	void pingpongBuffers(){
		int pvbo= vbo;
		int ptvbo= tvbo;
		vbo= ptvbo;
		tvbo= pvbo;

		int pvao= vao;
		int ptvao= tvao;
		vao= ptvao;
		tvao= pvao;
	}
	
	public static void loadLegacyModelviewProjection(int uniformlocaiton){
		final FloatBuffer mbuf= BufferUtils.createFloatBuffer(4*4);
		
		GL11.glGetFloat(GL11.GL_MODELVIEW_MATRIX, mbuf);
		mbuf.clear();
		final Matrix4f mv= (Matrix4f) new Matrix4f().load(mbuf);
		mbuf.clear();
		
		GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX, mbuf);
		mbuf.clear();
		final Matrix4f pr= (Matrix4f) new Matrix4f().load(mbuf);
		mbuf.clear();
		
		Matrix4f.mul(mv, pr, mv);
		mv.store(mbuf);
		mbuf.clear();
		GL20.glUniformMatrix4(uniformlocaiton, false, mbuf);
	}
}
