package particle;
import java.nio.FloatBuffer;
import java.util.Random;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;


//buf.put( (float)Math.sin(x*z/lside/10f)*lside/2f * (float)Math.sqrt(1f -x*x*4f/lsidesq -z*z*4f/lsidesq));
//					buf.put( (float)Math.sin(x*z/lside/10f)*lside/2f * (float)Math.sqrt(x*x*4f/lsidesq + z*z*4f/lsidesq));
//					buf.put((x-offs)*(z-offs)/lside*2f);

public abstract class Factory {
	static final FloatBuffer buf= BufferUtils.createFloatBuffer(3+3);
	int count, lside, lsidesq, lsiderecip, vbo;
	ParticleSystem psys;
	public float solspace;

	public void make(ParticleSystem psys, int count, int vbo, int tvbo){
		this.vbo= vbo;
		this.count= count;
		this.psys= psys;
		//checkexpand

		lside= (int) Math.pow(psys.vbocount, 1f/solspace);
		lsidesq= lside*lside;

		populateBuffer();
		
//		buf.position(0);
//		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
//		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buf, GL15.GL_DYNAMIC_DRAW);
//
//		buf.position(0);
//		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, tvbo);
//		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buf, GL15.GL_DYNAMIC_DRAW);
	}

	abstract void populateBuffer();

	public static void randomVelocitySphereical(int vbo){
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		int size= GL15.glGetBufferParameteri(GL15.GL_ARRAY_BUFFER, GL15.GL_BUFFER_SIZE);
//		if(buf.capacity()<size)
//			buf= BufferUtils.createByteBuffer(size);
		int count= size/ParticleSystem.STRIDE;
//		buf.clear();
//		buf.limit(size);
//		GL15.glGetBufferSubData(GL15.GL_ARRAY_BUFFER, 0, buf);
//		buf.clear();

		final Random r= new Random();
		for(int i=0; i<count; i++){
			buf.position(buf.position()+ParticleSystem.POSSIZE);

			final float mag= (float) r.nextFloat()*10,
					theta= (float) (r.nextFloat()*2*Math.PI),
					phi=  (float) ((r.nextFloat()-.5f)*Math.PI),
					costheta= (float) Math.cos(theta),
					sintheta= (float) Math.sin(theta),
					sinphi=   (float) Math.sin(phi),
					cosphi=   (float) Math.cos(phi);

			buf.put(mag*costheta*cosphi);
			buf.put(mag*sinphi);
			buf.put(mag*sintheta*cosphi);
		}
		buf.flip();
		//		buf.clear();
		GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, buf);
	}


	public static final Factory Cube= new Factory(){
		{solspace= 3;}
		@Override
		void populateBuffer() {
			for(float x=-lside/2; x<lside/2;x++){
				for(float y=-lside/2; y<lside/2; y++){
					for(float z=-lside/2; z<lside/2; z++){
						buf.put(x);
						buf.put(y);
						buf.put(z);

						buf.put(0);
						buf.put(0);
						buf.put(0);
					}			
				}
			}
		}
	};

	public static final Factory Sphere= new Factory(){
		{solspace= 3;}
		@Override
		void populateBuffer() {
			final float coe= (float)Math.pow( 4.*Math.PI/3. , 1./3.);
			float r= lside/(coe*coe);//4pi/3 r^3
			float rsq= r*r;
			for(float x=-r; x<r; x+=1.1f){
				for(float y=-r; y<r; y+=1.1f){
					for(float z=-r; z<r; z+=1.1f){
						//x*x + y*y + z*z = r*r
						//a= sqrt(r*r - b*b -c*c)
						//a valid if r*r > b*b+c*c
						float xyzsqs= x*x + y*y + z*z;
						if(xyzsqs<rsq){
							buf.put(x);
							buf.put(y);
							buf.put(z);

							buf.put(0);
							buf.put(0);
							buf.put(0);
						}
					}
				}
			}
			//			for(float theta=-lside/2; theta<lside/2;theta++){
			//				float cost= (float)Math.cos(theta);
			//				float sint= (float)Math.sin(theta);
			//				for(float phi=0; phi<lside; phi++){
			//						float cosp= (float)Math.cos(phi);
			//						buf.put(cost*cosp);
			//						buf.put((float)Math.sin(phi));
			//						buf.put(sint*cosp);
			//						buf.put(1);
			//
			//						buf.put(0);
			//						buf.put(v[1]);
			//						buf.put(v[2]);
			//						buf.put(0);
			//				}
			//			}
		}
	};
	public static final Factory Shell= new Factory(){
		{solspace= 3;}
		@Override
		void populateBuffer() {
			lside= (int) Math.pow(count, 1f/2f);
			lsidesq= lside*lside;
			for(float p=-1; p!=1;p=1){
				for(float x=-lside/2; x<lside/2;x+=2){
					for(float z=-lside/2; z<lside/2; z+=2){
						buf.put(x);
						buf.put( p* (float)Math.sqrt( lsidesq/4 -x*x -z*z  ) );
						buf.put(z);

						buf.put(0);
						buf.put(0);
						buf.put(0);
					}
				}
			}
		}
	};
	public static final Factory Sinusoid= new Factory(){
		{solspace= 2;}
		@Override
		void populateBuffer() {
			for(float x=-lside/2; x<lside/2;x++){
				for(float z=-lside/2; z<lside/2; z++){
					buf.put(x);
					buf.put((float)Math.sin(Math.PI*16f*x*z/lsidesq)*lside/4f);
					buf.put(z);

					buf.put(0);
					buf.put(0);
					buf.put(0);
				}	
			}
		}
	};

	public static final Factory SphereicalSinusiod= new Factory(){
		{solspace= 2;}
		@Override
		void populateBuffer() {
			for(float x=-lside/2; x<lside/2;x++){
				for(float z=-lside/2; z<lside/2; z++){
					buf.put(x);
					buf.put( (float)Math.sin(x*z/lside/10f)*lside/2f * (float)Math.sqrt(1f -x*x*4f/lsidesq -z*z*4f/lsidesq));
					buf.put(z);
					//						buf.put( (float)Math.sin(x*z/lside/10f)*lside/2f * (float)Math.sqrt(x*x*4f/lsidesq + z*z*4f/lsidesq));
					//						buf.put((x-offs)*(z-offs)/lside*2f);

					buf.put(0);
					buf.put(0);
					buf.put(0);
				}	
			}
		}
	};

	public static final Factory Planes= new Factory(){
		{solspace= 3;}
		@Override
		void populateBuffer() {
			lside= (int) Math.pow(count, 1f/3f);
			lsidesq= lside*lside;
			for(int i=-500; i!=500; i+=10){
				for(int j=-500; j!=500; j+=10){
					buf.put(j);
					buf.put(i);
					buf.put(0);


					buf.put(0);
					buf.put(0);
					buf.put(0);
				}
			}
		}
	};
}
