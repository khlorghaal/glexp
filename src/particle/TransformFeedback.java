package particle;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;


public abstract class TransformFeedback {
	
	int prog;
	final String src;
	public ParticleSystem psys= null;
	protected TransformFeedback(String src){
		this.src= "#version 150 core\n"+src;
	}
	
	public void init(){
		System.out.println("tp init");
		prog= GL20.glCreateProgram();
		final int tvsh= GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
		GL20.glShaderSource(tvsh, src);
		GL20.glCompileShader(tvsh);
		System.out.println(GL20.glGetShaderInfoLog(tvsh, 0xfff));
		GL20.glAttachShader(prog, tvsh);

		GL20.glBindAttribLocation(prog, 0, "pos");
		GL20.glBindAttribLocation(prog, 1, "v");

		GL30.glTransformFeedbackVaryings(prog, new String[]{"pout", "vout"}, GL30.GL_INTERLEAVED_ATTRIBS);

		GL20.glLinkProgram(prog);
		System.out.println(GL20.glGetProgramInfoLog(prog, 0xfff));
		GL11.glFinish();
		System.out.println("tp inited");		
	}
	
	public void invoke(Object... uniforms) {
		GL11.glEnable(GL30.GL_RASTERIZER_DISCARD);

		GL20.glUseProgram(prog);
		GL30.glBindVertexArray(psys.vao);

		setUniforms(uniforms);
		
		GL30.glBindBufferBase(GL30.GL_TRANSFORM_FEEDBACK_BUFFER, 0, psys.tvbo);

		GL30.glBeginTransformFeedback(GL11.GL_POINTS);
		//TODO 
		GL30.glEndTransformFeedback();

		GL11.glDisable(GL30.GL_RASTERIZER_DISCARD);
		GL11.glFlush();
	}
	abstract void setUniforms(Object... uniforms);
}
