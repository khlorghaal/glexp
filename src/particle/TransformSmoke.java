package particle;
import org.lwjgl.opengl.GL20;


public class TransformSmoke extends TransformFeedback{
	
	private TransformSmoke(){
		super(
						  "uniform float pT;"
						+ "uniform vec3 wind;"
//						+ "uniform float drift;"
//						+ "uniform float decay;"
						+ "const float drift= .2;"
						+ "const float decay= .96;"
						+ ""
						+ "in vec4 pos;"
						+ "in vec4 v;"
						+ ""
						+ "out vec4 pout;"
						+ "out vec4 vout;"
						+ ""
						+ "void main(){"
						+ ""
						+ "pout= pos+v*pT;"
						+ "if(length(v)>drift)"
						+ "	vout= v*decay;"
						+ "else"
						+ " vout= v;"
						+ "vout+= vec4(wind,0.);"
						+ "}"
						+ ""
				);
	}
	@Override
	public void init(){
		super.init();
		ptloc= GL20.glGetUniformLocation(prog, "pT");
		windloc= GL20.glGetUniformLocation(prog, "wind");
		driftloc= GL20.glGetUniformLocation(prog, "drift");
		decayloc= GL20.glGetUniformLocation(prog, "decay");
	}
	
	int ptloc, windloc, driftloc, decayloc;
	@Override
	void setUniforms(Object... uniforms) {
		GL20.glUniform1f(ptloc, (float) uniforms[0]);
		GL20.glUniform3f(windloc, (float)uniforms[2], (float)uniforms[3], (float)uniforms[4]);
		
	}

}
