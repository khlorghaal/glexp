package particle;
import org.lwjgl.opengl.GL20;


public class TransformOrbit extends TransformFeedback{	
	private int
	pTloc,
	curposloc,
	gloc,
	aloc;
	public TransformOrbit(){
		super(
						  "uniform float pT;"
						+ "uniform vec3 curpos;"
						+ "uniform float g;"
						+ "uniform vec3 a;"
						+ ""
						+ "in vec4 pos;"
						+ "in vec4 v;"
						+ ""
						+ "out vec4 pout;"
						+ "out vec4 vout;"
						+ ""
						+ "void main(){"
						+ ""
						+ "pout= pos+v*pT;"
						+ "float d= length(curpos - pos.xyz);"
						+ "if(d>.9) "
						+ "vout= ( v +vec4(a, 0.) +g*vec4((curpos-pos.xyz)/(d*d), 0.) );"
						+ "else "
						+ "vout= v;"
						+ "}"
						+ "");
	}
	@Override
	public void init(){
		super.init();
		pTloc= GL20.glGetUniformLocation(prog, "pT");
		curposloc= GL20.glGetUniformLocation(prog, "curpos");
		gloc= GL20.glGetUniformLocation(prog, "g");
		aloc= GL20.glGetUniformLocation(prog, "a");
	}
	
	@Override
	protected void setUniforms(Object... uniforms){
		GL20.glUniform3f(curposloc, 0, 0, 0);
		GL20.glUniform1f(pTloc, (float) uniforms[0]);
		GL20.glUniform1f(gloc, (float) uniforms[1]);
		GL20.glUniform3f(aloc, (float) uniforms[2], (float) uniforms[3], (float) uniforms[4]);
	}
}
