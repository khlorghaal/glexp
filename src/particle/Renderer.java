package particle;
import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import src.Environment;

public abstract class Renderer {
	public static final Renderer distal= new Renderer() {
		@Override
		String getFshsrc(){return 
				"#version 150 core\n"
				+ ""
				+ "uniform sampler2D density;"
				+ "uniform float zoom;"
				+ "flat in vec3 v;"
				+ ""
				+ "out vec4 color;"
				+ ""
				+ ""
				+"vec3 hsv2rgb(vec3 c)"
				+"{"
				+   " vec4 K = vec4(1., 2./3., 1./3., 3.);"
				+  "  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);"
				+"   return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);"
				+"}"
				+ ""
				+ ""
				+ "void main(){"
				+ ""
				+ "float fz= gl_FragCoord.z*2 -1;"
				+ "fz= (fz*fz*100000.);"
				+ "float depth= fz*.68/zoom;"
				+ "float r= length(v)/32.;"
				+ "vec3 c1= vec3(r,1,r);"
				+ "vec3 c2= vec3(r,r,1);"
				+ "vec3 rgb= mix(c1, c2, depth);"
				+ ""
				+ "color= vec4( rgb, 1.);"
				+ "color= vec4(1.);"
				+ ""
				+ "}";}
	};

	
	String getVshsrc(){return "#version 150 core\n"
			+ ""
			+ "uniform mat4 cam;"
			+ "uniform float vcolormul;"
			+ ""
			+ "in vec3 pos;"
			+ "in vec3 vin;"
			+ "flat out vec3 v;"
			+ ""
			+ "void main(){" 
			+ "  v= vin*vcolormul;"
			+ "  gl_Position= cam*vec4(pos, 1.);"
			+ "	 gl_PointSize= (.1 / (gl_Position.z))+2.;"
//			+ "  gl_Position.xy/= gl_Position.z;"
			+ "  gl_Position.z/=100.;"
//			+ "  gl_Position.w= -1/gl_Position.z;"
//			+ "  gl_Position.w= 1;"
			+ "}";
	}

	abstract String getFshsrc();
	
	
	int prog, camloc, zoomloc, vcolormulloc;
	public ParticleSystem psys;
	
	public void init(){
		System.out.println("rp start");
		prog= GL20.glCreateProgram();
		int vsh= GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
		int fsh= GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);
		GL20.glShaderSource(vsh, getVshsrc());
		GL20.glCompileShader(vsh);
		System.out.println(GL20.glGetShaderInfoLog(vsh, 0xfff));

		GL20.glShaderSource(fsh, getFshsrc());
		GL20.glCompileShader(fsh);
		System.out.println(GL20.glGetShaderInfoLog(fsh, 0xfff));

		//		if(type==Primitive.CUBE){
		//			int gsh= GL20.glCreateShader(GL32.GL_GEOMETRY_SHADER);
		//			GL20.glShaderSource(gsh, 
		//					"#version 150 core\n"
		//					+ "in vec4 v;"
		//					+ "flat out {vec4 v;}outdat;"
		//					+ ""
		//					+ "layout(points) in p[];"
		//					+ "layout(triangles) out;"
		//					+ ""
		//					+ "void main(){"
		//					+ "outdat.v= v;"
		//					+ ""
		//					+ ""
		//					+ "EndPrimitive();"
		//					+ "}"
		//					);
		//			GL20.glCompileShader(gsh);
		//			System.out.println(GL20.glGetShaderInfoLog(fsh, 0xfff));
		//			GL20.glAttachShader(prog, gsh);
		//		}

		GL20.glAttachShader(prog, vsh);
		GL20.glAttachShader(prog, fsh);
		GL20.glBindAttribLocation(prog, 0, "pos");
		GL20.glBindAttribLocation(prog, 1, "vin");
		//uniloc
		GL20.glLinkProgram(prog);

		//To add uniforms, pile them onto here
		//Its much more convenient than polymorphism
		camloc= GL20.glGetUniformLocation(prog, "cam");
		zoomloc= GL20.glGetUniformLocation(prog, "zoom");
		vcolormulloc= GL20.glGetUniformLocation(prog, "vcolormul");
		
		System.out.println(GL20.glGetProgramInfoLog(prog, 0xfff));
		GL11.glFinish();
		Environment.checkerr();
		System.out.println("rp done");		
	}
	
	public void setUniforms(Object... unis){
		GL20.glUniformMatrix4(camloc, false, (FloatBuffer) unis[0]);
	}
}
