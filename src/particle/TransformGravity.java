package particle;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;


public class TransformGravity extends TransformFeedback {
	int posTex;
	int countloc, ptloc, gloc;

	public TransformGravity(){
		super(
			  "uniform float pT;"
			+ "uniform samplerBuffer others;"
			+ "uniform int count;"
			+ "uniform float g;"
			+ ""
			+ "in vec4 pos;"
			+ "in vec4 v;"
			+ ""
			+ "out vec4 pout;"
			+ "out vec4 vout;"
			+ ""
			+ "void main(){"
			+ ""
			+ "pout= pos+v*pT;"
			+ "vout= v;"
			+ ""
			+ "int collided= -1;"
			+ "for(int i=0; i!= count*2; i+=2){"
			+ "vec3 d= texelFetch(others, i).xyz - pos.xyz;"
			+ "float dl= length(d);"
			+ "if(dl>.1){ vout+= g*vec4((d)/(dl*dl), 0.); }"
			+ "else if(dl!=0){ collided=i; }"
			+ "}"
			+ "if(collided!=-1){ vout= texelFetch(others, collided+1);}"
			+ "vout*=.94;"
			+ "}"
			+ "");
	}
	@Override
	public void init(){
		super.init();
		
		ptloc= GL20.glGetUniformLocation(prog, "pT");
		countloc= GL20.glGetUniformLocation(prog, "count");
		gloc= GL20.glGetUniformLocation(prog, "g");

		GL20.glUseProgram(prog);
		GL20.glUniform1i(GL20.glGetUniformLocation(prog, "others"), 0);
		posTex= GL11.glGenTextures();
	}
	
	@Override
	void setUniforms(Object... uniforms){
		if(psys.vbocount*psys.vbocount>512000000){
			System.err.println("Too many particles for gravity");
			GL20.glUniform1i(countloc, 0);
			return;
		}
		GL20.glUniform1i(countloc, psys.vbocount);
		GL20.glUniform1f(ptloc, (float)uniforms[0]);
		GL20.glUniform1f(gloc, (float)uniforms[1]/8f);

		GL13.glActiveTexture(GL13.GL_TEXTURE0);
//		GL15.glBindBuffer(GL31.GL_TEXTURE_BUFFER, vbo);
		GL11.glBindTexture(GL31.GL_TEXTURE_BUFFER, posTex);
		GL31.glTexBuffer(GL31.GL_TEXTURE_BUFFER, GL30.GL_RGBA32F, psys.vbo);
		
	}
}
