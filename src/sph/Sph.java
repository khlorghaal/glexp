package sph;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import src.Environment;
import src.IProgram;

public class Sph implements IProgram{
	public static void main(String[] args){
		new Environment(new Sph()).run();
	}
	
	static final int count= 1000000, stride= 4*2*2;
	int progr, progt;
	int[] vbo= new int[2], vao= new int[2];
	int ping=0, pong=1;
	FloatBuffer fb= BufferUtils.createFloatBuffer(count*2*2);
	@Override
	public void init(){
		int e= (int)Math.sqrt(count);
		for(int x=-e/2; x!=e/2; x++){
			for(int y=-e/2; y!=e/2; y++){
				fb.put((float)x/e);
				fb.put((float)y/e);
				fb.put(0);
				fb.put(0);
			}
		}
		fb.clear();
		
		for(int i=0; i!=2; i++){
			vbo[i]= GL15.glGenBuffers();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo[i]);
			fb.clear();
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fb, GL15.GL_STATIC_DRAW);
			
			vao[i]= GL30.glGenVertexArrays();
			GL30.glBindVertexArray(vao[i]);
			GL20.glEnableVertexAttribArray(0);
			GL20.glEnableVertexAttribArray(1);
			GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, stride, 0);
			GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, stride, 2*4);
			GL30.glBindVertexArray(0);
		}
		
		progr= Environment.program(
				"#version 440\n"
						+ "layout(location=0) in vec2 posin;"
						+ "layout(location=1) in vec2 vin;"
						+ "flat out vec2 v;"
						+ "void main(){"
						+ "    gl_Position= vec4(posin, 0,1);"
						+ "    v= vin;"
						+ "}", 
						"#version 440\n"
								+ "flat in vec2 v;"
								+ "out vec4 color;"
								+ "void main(){"
								+ "    color= vec4(1,1,1,1);"
								+ "}");
		
		progt= Environment.program(
				"#version 440\n"
						+ "layout(location=0) in vec2 posin;"
						+ "layout(location=1) in vec2 vin;"
						+ "out vec2 pos;"
						+ "out vec2 v;"
						+ "void main(){"
						+ "    vec2 g= normalize(-posin);"
						+ "    v= vin + 1./sqrt(abs(g))*sign(g)*.0001;"
						+ "    pos= posin+v;"
						+ "}");
		GL30.glTransformFeedbackVaryings(progt, new String[]{"pos","v"}, GL30.GL_INTERLEAVED_ATTRIBS);
		GL20.glLinkProgram(progt);
		
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
		GL11.glPointSize(1);
	}
	
	@Override
	public void render(Matrix4f cam, Object[] unis){
		GL20.glUseProgram(progr);
		GL30.glBindVertexArray(vao[ping]);
		GL11.glDrawArrays(GL11.GL_POINTS, 0, count);
	}
	
	@Override
	public void transform(Object[] unis){
		GL20.glUseProgram(progt);
		GL11.glEnable(GL30.GL_RASTERIZER_DISCARD);
		GL30.glBindVertexArray(vao[ping]);
		GL30.glBindBufferBase(GL30.GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[pong]);
		GL30.glBeginTransformFeedback(GL11.GL_POINTS);
		GL11.glDrawArrays(GL11.GL_POINTS, 0, count);
		GL30.glEndTransformFeedback();
		GL11.glDisable(GL30.GL_RASTERIZER_DISCARD);
		
		ping= ping==0?1:0;
		pong= pong==0?1:0;		
	}
	
}
